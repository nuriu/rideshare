﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RideShare.Persistence.Migrations
{
    public partial class AddedLocationsForDetailedSearch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "from_location_id",
                table: "trips",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_location_id",
                table: "trips",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "locations",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    x = table.Column<int>(type: "integer", nullable: false),
                    y = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_locations", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_trips_from_location_id",
                table: "trips",
                column: "from_location_id");

            migrationBuilder.CreateIndex(
                name: "ix_trips_to_location_id",
                table: "trips",
                column: "to_location_id");

            migrationBuilder.AddForeignKey(
                name: "FK_trips_locations_from_location_id",
                table: "trips",
                column: "from_location_id",
                principalTable: "locations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_trips_locations_to_location_id",
                table: "trips",
                column: "to_location_id",
                principalTable: "locations",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_trips_locations_from_location_id",
                table: "trips");

            migrationBuilder.DropForeignKey(
                name: "FK_trips_locations_to_location_id",
                table: "trips");

            migrationBuilder.DropTable(
                name: "locations");

            migrationBuilder.DropIndex(
                name: "ix_trips_from_location_id",
                table: "trips");

            migrationBuilder.DropIndex(
                name: "ix_trips_to_location_id",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "from_location_id",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "to_location_id",
                table: "trips");
        }
    }
}
