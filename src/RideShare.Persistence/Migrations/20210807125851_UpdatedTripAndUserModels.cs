﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RideShare.Persistence.Migrations
{
    public partial class UpdatedTripAndUserModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "available_seats_in_vehicle",
                table: "users");

            migrationBuilder.AddColumn<DateTime>(
                name: "departion_time",
                table: "trips",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "trips",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "seats_in_vehicle",
                table: "trips",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "status",
                table: "trips",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "departion_time",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "description",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "seats_in_vehicle",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "status",
                table: "trips");

            migrationBuilder.AddColumn<int>(
                name: "available_seats_in_vehicle",
                table: "users",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
