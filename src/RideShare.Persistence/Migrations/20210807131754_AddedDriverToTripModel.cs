﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RideShare.Persistence.Migrations
{
    public partial class AddedDriverToTripModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "driver_id",
                table: "trips",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_trips_driver_id",
                table: "trips",
                column: "driver_id");

            migrationBuilder.AddForeignKey(
                name: "FK_trips_users_driver_id",
                table: "trips",
                column: "driver_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_trips_users_driver_id",
                table: "trips");

            migrationBuilder.DropIndex(
                name: "ix_trips_driver_id",
                table: "trips");

            migrationBuilder.DropColumn(
                name: "driver_id",
                table: "trips");
        }
    }
}
