using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RideShare.Core.Entities;

namespace RideShare.Persistence.Configurations
{
    public class TripConfiguration : IEntityTypeConfiguration<Trip>
    {
        public void Configure(EntityTypeBuilder<Trip> builder)
        {
            builder.HasMany(t => t.Users).WithMany(u => u.Trips);
            builder.HasOne(t => t.Driver);
            builder.HasOne(t => t.FromLocation);
            builder.HasOne(t => t.ToLocation);

            builder.HasKey(t => t.Id);

            builder.Property(t => t.From)
                   .IsRequired()
                   .HasMaxLength(128);

            builder.Property(t => t.To)
                   .IsRequired()
                   .HasMaxLength(128);

            builder.Property(t => t.Description)
                   .HasMaxLength(500);

            builder.Property(t => t.DepartionTime)
                   .IsRequired();

            builder.Property(u => u.SeatsInVehicle)
                   .IsRequired();
        }
    }
}
