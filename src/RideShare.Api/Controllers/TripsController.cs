using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RideShare.Api.Filters;
using RideShare.Application.Commands.Trips;
using RideShare.Application.Queries.Trips;
using RideShare.Core.Entities;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RideShare.Api.Controllers
{
    [ExceptionFilter]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("[controller]")]
    public class TripsController : ControllerBase
    {
        protected readonly IMediator _mediator;

        public TripsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Trip>), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ICollection<Trip>> Get([FromQuery] SearchQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpGet("detailed-search")]
        [ProducesResponseType(typeof(ICollection<Trip>), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ICollection<Trip>> GetDetailed([FromQuery] DetailedSearchQuery query)
        {
            return await _mediator.Send(query);
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(Trip), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ActionResult<Trip>> Post([FromBody] CreateCommand data)
        {
            data.DriverId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            return await _mediator.Send(data);
        }

        [HttpPost("ChangeStatus")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(Trip), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ActionResult<Trip>> ChangeTripStatus([FromBody] ChangeStatusCommand data)
        {
            data.DriverId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            return await _mediator.Send(data);
        }

        [HttpPost("Join")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(Trip), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ActionResult<Trip>> JoinTrip([FromBody] JoinCommand data)
        {
            data.UserId = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            return await _mediator.Send(data);
        }
    }
}
