using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RideShare.Api.Filters;
using RideShare.Application.Commands.Users;
using RideShare.Application.Queries.Users;
using RideShare.Core.Entities;
using System.Net.Mime;
using System.Threading.Tasks;

namespace RideShare.Api.Controllers
{
    [ExceptionFilter]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        protected readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AllowAnonymous]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ActionResult<User>> Post([FromBody] CreateCommand data)
        {
            return await _mediator.Send(data);
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ValidationProblemDetails), 400)]
        public async Task<ActionResult<string>> Token([FromBody] AuthQuery query)
        {
            return await _mediator.Send(query);
        }
    }
}
