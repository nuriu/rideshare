using System;
using System.Runtime.Serialization;

namespace RideShare.Core.Exceptions
{
    public class TripFullException : RideShareException
    {
        protected TripFullException() : base()
        {
        }
        public TripFullException(Guid tripId) : base($"All seats are taken in trip with specified id: {tripId}")
        {
        }

        public TripFullException(string from, string to) : base($"All seats are taken in trips between specified destinations: {from} - {to}")
        {
        }

        protected TripFullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public TripFullException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
