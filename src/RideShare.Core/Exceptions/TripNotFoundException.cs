using System;
using System.Runtime.Serialization;

namespace RideShare.Core.Exceptions
{
    public class TripNotFoundException : RideShareException
    {
        protected TripNotFoundException() : base()
        {
        }
        public TripNotFoundException(Guid tripId) : base($"Could not found any trip with specified id: {tripId}")
        {
        }

        public TripNotFoundException(string from, string to) : base($"Could not found any trip between specified destinations: {from} - {to}")
        {
        }

        protected TripNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public TripNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
