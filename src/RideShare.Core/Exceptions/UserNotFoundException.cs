using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RideShare.Core.Exceptions
{
    public class UserNotFoundException : RideShareException
    {
        protected UserNotFoundException() : base()
        {
        }

        public UserNotFoundException(string username) : base($"Could not found any user with specified id: {username}")
        {
        }

        public UserNotFoundException(IEnumerable<string> userIds) : base("Could not found any user with specified usernames.")
        {
        }

        protected UserNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public UserNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
