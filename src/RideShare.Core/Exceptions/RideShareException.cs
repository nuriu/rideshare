using System;
using System.Runtime.Serialization;

namespace RideShare.Core.Exceptions
{
    public abstract class RideShareException : Exception
    {
        protected RideShareException() : base()
        {
        }

        protected RideShareException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected RideShareException(string message) : base(message)
        {
        }

        protected RideShareException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
