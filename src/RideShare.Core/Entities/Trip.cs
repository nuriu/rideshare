using System;
using System.Collections.Generic;

namespace RideShare.Core.Entities
{
    public class Trip : BaseEntity
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Description { get; set; }
        public DateTime DepartionTime { get; set; }
        public bool Status { get; set; }
        public int SeatsInVehicle { get; set; }

        // for detailed search
        public virtual Location FromLocation { get; set; }
        public virtual Location ToLocation { get; set; }

        public virtual User Driver { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
