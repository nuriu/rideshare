using System.Collections.Generic;

namespace RideShare.Core.Entities
{
    public class User : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Trip> Trips { get; set; }
    }
}
