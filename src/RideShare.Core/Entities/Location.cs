namespace RideShare.Core.Entities
{
    public class Location : BaseEntity
    {
        public string Name { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
    }
}
