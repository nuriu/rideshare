using System.Linq;
using System.Text.RegularExpressions;

namespace RideShare.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) return input;

            return Regex.Match(input, @"^_+") + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLowerInvariant();
        }

        public static bool ShouldNotContainDigits(this string input)
        {
            return !input.Any(char.IsDigit);
        }

        public static bool ShouldContainDigits(this string input)
        {
            return input.Any(char.IsDigit);
        }
    }
}
