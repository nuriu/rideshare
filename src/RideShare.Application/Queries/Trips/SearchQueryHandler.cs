using MediatR;
using RideShare.Core.Entities;
using RideShare.Core.Exceptions;
using RideShare.Persistence.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Queries.Trips
{
    public class SearchQueryHandler : IRequestHandler<SearchQuery, ICollection<Trip>>
    {
        private readonly RideShareContext _context;

        public SearchQueryHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Trip>> Handle(SearchQuery request, CancellationToken cancellationToken)
        {
            var trips = _context.Trips.Where(x => x.Status == true);

            if (!string.IsNullOrEmpty(request.From))
            {
                trips = trips.Where(x => x.From == request.From);
            }

            if (!string.IsNullOrEmpty(request.To))
            {
                trips = trips.Where(x => x.To == request.To);
            }

            if (!trips.Any())

            {
                throw new TripNotFoundException(request.From, request.To);
            }

            return trips.ToList();
        }
    }
}
