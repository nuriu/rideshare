using MediatR;
using RideShare.Core.Entities;
using RideShare.Core.Exceptions;
using RideShare.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Queries.Trips
{
    public class DetailedSearchQueryHandler : IRequestHandler<DetailedSearchQuery, ICollection<Trip>>
    {
        private readonly RideShareContext _context;

        public DetailedSearchQueryHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Trip>> Handle(DetailedSearchQuery request, CancellationToken cancellationToken)
        {
            var from = _context.Locations.FirstOrDefault(x => x.Name == request.From);
            var to = _context.Locations.FirstOrDefault(x => x.Name == request.To);

            var trips = _context.Trips.Where(x => x.Status == true).ToList();
            trips = trips.Where(x => IsInBetween(from, to, x.FromLocation, x.ToLocation)).ToList();
            if (!trips.Any())
            {
                throw new TripNotFoundException(request.From, request.To);
            }

            return trips.ToList();
        }

        private bool IsInBetween(Location from, Location to,
                                 Location destFrom, Location destTo)
        {
            return Math.Min(from.X, to.X) <= Math.Min(destFrom.X, destTo.X)
                && Math.Max(from.X, to.X) >= Math.Max(destFrom.X, destTo.X)
                && Math.Min(from.Y, to.Y) <= Math.Min(destFrom.Y, destTo.Y)
                && Math.Max(from.Y, to.Y) >= Math.Max(destFrom.Y, destTo.Y);
        }
    }
}
