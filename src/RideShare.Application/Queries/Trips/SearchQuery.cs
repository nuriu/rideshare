using FluentValidation;
using MediatR;
using RideShare.Core.Entities;
using RideShare.Core.Extensions;
using System.Collections.Generic;

namespace RideShare.Application.Queries.Trips
{
    public class SearchQuery : IRequest<ICollection<Trip>>
    {
        public string From { get; set; }
        public string To { get; set; }
    }

    public class SearchQueryValidator : AbstractValidator<SearchQuery>
    {
        public SearchQueryValidator()
        {
            RuleFor(q => q.From)
                .Cascade(CascadeMode.Stop)
                .MaximumLength(50);

            RuleFor(q => q.To)
                .Cascade(CascadeMode.Stop)
                .MaximumLength(50);
        }
    }
}
