using FluentValidation;
using MediatR;
using RideShare.Core.Entities;
using System.Collections.Generic;

namespace RideShare.Application.Queries.Trips
{
    public class DetailedSearchQuery : IRequest<ICollection<Trip>>
    {
        public string From { get; set; }
        public string To { get; set; }
    }

    public class DetailedSearchQueryValidator : AbstractValidator<DetailedSearchQuery>
    {
        public DetailedSearchQueryValidator()
        {
            RuleFor(q => q.From)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(q => q.To)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
