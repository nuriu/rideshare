using FluentValidation;
using MediatR;
using RideShare.Core.Entities;
using RideShare.Core.Extensions;
using System;

namespace RideShare.Application.Commands.Trips
{
    public class CreateCommand : IRequest<Trip>
    {
        public Guid DriverId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Description { get; set; }
        public DateTime DepartionTime { get; set; }

        public int FromX { get; set; }
        public int FromY { get; set; }
        public int ToX { get; set; }
        public int ToY { get; set; }

        public bool Status { get; set; }
        public int SeatsInVehicle { get; set; }
    }

    public class CreateCommandValidator : AbstractValidator<CreateCommand>
    {
        public CreateCommandValidator()
        {
            RuleFor(c => c.From)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Must(ShouldNotContainDigits)
                .WithMessage("From alanı numara içermemeli.")
                .MaximumLength(50);

            RuleFor(c => c.To)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Must(ShouldNotContainDigits)
                .WithMessage("To alanı numara içermemeli.")
                .MaximumLength(50);

            RuleFor(c => c.Description)
                .Cascade(CascadeMode.Stop)
                .MaximumLength(500);

            RuleFor(c => c.DepartionTime)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .GreaterThanOrEqualTo(DateTime.UtcNow.AddHours(-12));

            RuleFor(c => c.SeatsInVehicle)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .GreaterThan(1);
        }

        private bool ShouldNotContainDigits(string s) => s.ShouldNotContainDigits();
    }
}
