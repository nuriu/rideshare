using MediatR;
using Microsoft.EntityFrameworkCore;
using RideShare.Core.Entities;
using RideShare.Core.Exceptions;
using RideShare.Persistence.Contexts;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Commands.Trips
{
    public class JoinCommandHandler : IRequestHandler<JoinCommand, Trip>
    {
        private readonly RideShareContext _context;

        public JoinCommandHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<Trip> Handle(JoinCommand request, CancellationToken cancellationToken)
        {
            var trip = await _context.Trips.FindAsync(request.TripId);
            var user = await _context.Users.FindAsync(request.UserId);

            if (trip == null || // if we couldn't found trip
                trip.Status != true || // or trip is not active
                trip.Users.Any(x => x.Id == request.UserId) || // or user already joined in
                trip.Driver.Id == request.UserId) // or user is the driver
            {
                throw new TripNotFoundException(request.TripId);
            }

            if (trip.Users.Count >= trip.SeatsInVehicle - 1) //  or vehicle is full (- 1 for driver)
            {
                throw new TripFullException(request.TripId);
            }

            trip.Users.Add(user);
            _context.Entry(trip).State = EntityState.Modified;

            var success = await _context.SaveChangesAsync(cancellationToken) > 0;

            if (!success)
                throw new Exception("Problem saving changes.");

            return trip;
        }
    }
}
