using MediatR;
using RideShare.Core.Entities;
using RideShare.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Commands.Trips
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, Trip>
    {
        private readonly RideShareContext _context;

        public CreateCommandHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<Trip> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FindAsync(request.DriverId);
            var trip = new Trip
            {
                From = request.From,
                To = request.To,
                DepartionTime = request.DepartionTime,
                Description = request.Description,
                SeatsInVehicle = request.SeatsInVehicle,
                FromLocation = new Location { Name = request.From, X = request.FromX, Y = request.FromY },
                ToLocation = new Location { Name = request.To, X = request.ToX, Y = request.ToY },
                Status = request.Status,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Driver = user
            };

            _context.Trips.Add(trip);

            var success = await _context.SaveChangesAsync(cancellationToken) > 0;

            if (!success)
                throw new Exception("Problem saving changes.");

            return trip;
        }
    }
}
