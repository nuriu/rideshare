using FluentValidation;
using MediatR;
using RideShare.Core.Entities;
using System;

namespace RideShare.Application.Commands.Trips
{
    public class JoinCommand : IRequest<Trip>
    {
        public Guid TripId { get; set; }
        public Guid UserId { get; set; }
    }

    public class JoinCommandValidator : AbstractValidator<JoinCommand>
    {
        public JoinCommandValidator()
        {
            RuleFor(c => c.TripId)
                .Cascade(CascadeMode.Stop)
                .NotEmpty();
        }
    }
}
