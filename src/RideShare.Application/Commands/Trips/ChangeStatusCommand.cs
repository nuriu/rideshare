using FluentValidation;
using MediatR;
using RideShare.Core.Entities;
using System;

namespace RideShare.Application.Commands.Trips
{
    public class ChangeStatusCommand : IRequest<Trip>
    {
        public Guid TripId { get; set; }
        public Guid DriverId { get; set; }
        public bool NewStatus { get; set; }
    }

    public class ChangeStatusCommandValidator : AbstractValidator<ChangeStatusCommand>
    {
        public ChangeStatusCommandValidator()
        {
            RuleFor(c => c.TripId)
                .Cascade(CascadeMode.Stop)
                .NotEmpty();
        }
    }
}
