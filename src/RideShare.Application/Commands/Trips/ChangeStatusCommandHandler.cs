using MediatR;
using Microsoft.EntityFrameworkCore;
using RideShare.Core.Entities;
using RideShare.Core.Exceptions;
using RideShare.Persistence.Contexts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Commands.Trips
{
    public class ChangeStatusCommandHandler : IRequestHandler<ChangeStatusCommand, Trip>
    {
        private readonly RideShareContext _context;

        public ChangeStatusCommandHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<Trip> Handle(ChangeStatusCommand request, CancellationToken cancellationToken)
        {
            var trip = await _context.Trips.FindAsync(request.TripId);

            // if user isn't the driver or we couldn't found trip
            if (trip.Driver.Id != request.DriverId || trip == null)
            {
                throw new TripNotFoundException(request.TripId);
            }

            trip.Status = request.NewStatus;
            _context.Entry(trip).State = EntityState.Modified;

            var success = await _context.SaveChangesAsync(cancellationToken) > 0;

            if (!success)
                throw new Exception("Problem saving changes.");

            return trip;
        }
    }
}
