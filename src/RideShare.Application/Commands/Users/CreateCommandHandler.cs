using MediatR;
using RideShare.Core.Entities;
using RideShare.Persistence.Contexts;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RideShare.Application.Commands.Users
{
    public class CreateCommandHandler : IRequestHandler<CreateCommand, User>
    {
        private readonly RideShareContext _context;

        public CreateCommandHandler(RideShareContext context)
        {
            _context = context;
        }

        public async Task<User> Handle(CreateCommand request, CancellationToken cancellationToken)
        {
            var user = new User
            {
                Username = request.Username,
                Password = request.Password,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            _context.Users.Add(user);

            var success = await _context.SaveChangesAsync(cancellationToken) > 0;

            if (!success)
                throw new Exception("Problem saving changes.");

            return user;
        }
    }
}
