** 1. Bölüm **

Kullanıcıların A şehirinden B şehirine şahsi arabaları ile seyehat ederken yolcu bulabileceği API'nin aşağıda yazılı senaryolarının end pointlerini yazar mısınız?

    - [x] Kullanıcı sisteme seyahat planını Nereden, Nereye, Tarih ve Açıklama, Koltuk Sayısı bilgileri ile ekleyebilmeli
    - [x] Kullanıcı tanımladığı seyahat planını yayına alabilmeli ve yayından kaldırabilmeli
    - [x] Kullanıcılar sistemdeki yayında olan seyahat planlarını Nereden ve Nereye bilgileri ile aratabilmeli
    - [x] Kullanıcılar yayında olan seyehat planlarına "Koltuk Sayısı" dolana kadar katılım isteği gönderebilmeli

** 2. Bölüm **

AdessoRideShare rest api, 500 km yükseklik ve 1000 km genişliğinde dikdörtgen seklinde bir ada da hizmet vermektedir.

Ada devlet üzerinde her şehir 50 km lik karelerden oluşmaktadır.

Kara yolları ise bu şehirlerin aralarında oluşan sınırlarından geçmektedir.

    - [x] Kullanıcılar Nereden ve Nereye bilgileri ile seyahat aradığında bu güzergahtan geçen tüm yayında olan seyahat planlarını bulabilmeli
