FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

EXPOSE 5000
ENV ASPNETCORE_URLS http://+:5000

COPY *.sln .
COPY src/RideShare.Api/* ./src/RideShare.Api/
COPY src/RideShare.Application/* ./src/RideShare.Application/
COPY src/RideShare.Core/* ./src/RideShare.Core/
COPY src/RideShare.Persistence/* ./src/RideShare.Persistence/
COPY tests/RideShare.Tests/* ./tests/RideShare.Tests/
RUN dotnet restore

RUN dotnet publish -o RideShare

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /app/RideShare .
ENTRYPOINT ["dotnet", "RideShare.Api.dll"]
